#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

void download_extract() {
    pid_t pid = fork();

    if (pid == 0) {
        // Child process
        system("kaggle datasets download -d bryanb/fifa-player-stats-database && unzip -o fifa-player-stats-database.zip");
        exit(0);
    } else if (pid > 0) {
        // Parent process
        wait(NULL);
    }
}

void display() {
    system("awk -F\",\" '($3 <= 25 && $7 >= 75 && $9 != \"Manchester City\") { printf \"%-20s %-20s %-20s %-20s %-20s %-20s\\n\", $2, $9, $3, $8, $5, $4 }' FIFA23_*.csv");
}

int main() {
    download_extract();
    display();
    
    return 0;
}
