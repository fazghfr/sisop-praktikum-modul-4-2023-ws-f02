#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <sys/xattr.h>
#include <sys/wait.h>
#include <pthread.h>


static const char *dirpath = "/home/arfikra/Documents";

void actlog(int flag, char *cmd, const char *desc)
{
    char timebuffer[80], mode[7];
    FILE *prog_log = fopen("/home/arfikra/fs_module.log", "a");

    time_t tnow = time(NULL);
    struct tm *time = localtime(&tnow);

    if(!flag) 
    {
        strcpy(mode, "REPORT");
    }
    else
    {
        strcpy(mode, "FLAG");
    }
    strftime(timebuffer, 90, "%y%m%d-%H:%M:%S", time);
    fprintf(prog_log, "%s::%s::%s::%s\n",mode,timebuffer, cmd, desc);

    fclose(prog_log);
}

void mod(char *path)
{
    FILE *file = fopen(path, "rb");
    int count = 0;
    char tarpath[2000];

    sprintf(tarpath, "%s.%03d", path, count);
    void *buffer = malloc(1024);

    while (1)
    {
        size_t size = fread(buffer, 1, 1024, file);
        if (!size) break;

        FILE *fetch = fopen(tarpath, "w");
        fwrite(buffer, 1, size, fetch);
        fclose(fetch);
        count++;
        sprintf(tarpath, "%s.%03d", path, count);
    }
    free(buffer);
    fclose(file);
    remove(path);
}

void unmod(char *path)
{
    DIR *dir = opendir(path);
    struct dirent *entry;
    struct stat sb;
    while ((entry = readdir(dir)))
    {
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

        char tarpath[300];
        snprintf(tarpath, sizeof(tarpath), "%s/%s", path, entry->d_name);
        if (stat(tarpath, &sb) == -1) continue;

        if (S_ISDIR(sb.st_mode))
        {
            unmod(tarpath);
        }
        else if (S_ISREG(sb.st_mode) && strlen(tarpath) > 3 && !strcmp(tarpath + strlen(tarpath) - 3, "000"))
        {
            int count = 0;
            char dest[300], temp[400];
            memset(dest, '\0', sizeof(dest));
            memset(temp, '\0', sizeof(temp));
            strncpy(dest, tarpath, strlen(tarpath) - 4);
                
            while(1)
            {
                if (count < 10) snprintf(temp, sizeof(temp), "%s.00%d", dest, count);
                else if (count < 100) snprintf(temp, sizeof(temp), "%s.0%d", dest, count);
                else snprintf(temp, sizeof(temp), "%s.%d", dest, count);

                if (stat(temp, &sb)) break;
                FILE *destfile = fopen(dest, "ab");
                FILE *tempfile = fopen(temp, "rb");
                if (destfile && tempfile) 
                {
                    char buffer[1024];
                    size_t read_bytes;
                    while ((read_bytes = fread(buffer, 1, sizeof(buffer), tempfile)) > 0) 
                    {
                        fwrite(buffer, 1, read_bytes, destfile);
                    }
                }
                if (tempfile) fclose(tempfile);
                if (destfile) fclose(destfile);
                remove(temp);
                count++;
            }
        }
    }
    closedir(dir);
}


void modularize(char *path)
{
    struct dirent *ep;
    DIR *dp; 
    dp = opendir(path);

    if(!dp) return;

    char dir[1000], file[1000];

    while ((ep = readdir (dp)))
    {
      if (!strcmp(ep->d_name, ".") || !strcmp(ep->d_name, "..")) continue;
      if(ep->d_type == DT_DIR)
      {
        sprintf(dir, "%s/%s", path, ep->d_name);
        modularize(dir);
      }
      else if (ep->d_type == DT_REG)
      {
        sprintf(file, "%s/%s", path, ep->d_name);
        mod(file);
      }
    }
    closedir(dp);
}

static int _getattr(const char *path, struct stat *stbuf) 
{
  char *modular = strstr(path, "module_");

  int res;
  char fpath[1000];

  sprintf(fpath, "%s%s", dirpath, path);
  res = lstat(fpath, stbuf);

  if (res == -1) 
  {
    if (!modular || !strstr(modular, "/")) 
    {
      return -errno;
    } 
    else 
    {
      sprintf(fpath, "%s%s.000", dirpath, path);
      lstat(fpath, stbuf);

      int count = 0, sizeCount = 0;
      struct stat st;

      while(!stat(fpath, &st)) 
      {
        count++;
        sprintf(fpath, "%s%s.%03d", dirpath, path, count);
        sizeCount += st.st_size;
      }
      stbuf->st_size = sizeCount;
    }
  }

  return 0;
}

static int _readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) 
{
  char *modular = strstr(path, "module_");
  
  char fpath[1000];
  if (!strcmp(path, "/")) 
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  } 
  else 
  {
    sprintf(fpath, "%s%s", dirpath, path);
  }

  int res = 0;

  struct dirent *ep;
  (void) offset;
  (void) fi;
  DIR *dp;
  dp = opendir(fpath);
  if (!dp) return -errno;

  while ((ep = readdir(dp))) 
  {
    struct stat st;
    memset(&st, 0, sizeof(st));
    st.st_ino = ep->d_ino;
    st.st_mode = ep->d_type << 12;

    int d_len = strlen(ep->d_name);
    if (modular && ep->d_type == DT_REG) 
    {
      if (!strcmp(ep->d_name+(d_len-4), ".000")) 
      {
        ep->d_name[d_len-4] = '\0';
        res = (filler(buf, ep->d_name, &st, 0));
      }
    } 
    else 
    {
      res = (filler(buf, ep->d_name, &st, 0));
    } 
    if (res != 0) break;
  }

  closedir(dp);
  if (res == -1) return -errno;
  actlog(0,"LS", fpath);
  return 0;
}


static int _mkdir(const char *path, mode_t mode) 
{
  char *modular = strstr(path, "module_");

  char fpath[1000];
  sprintf(fpath, "%s%s", dirpath, path);
  
  int res;
  res = mkdir(fpath, mode);

  if (res == -1) return -errno;
  if (modular)
  {
    modularize(fpath);
  }
  
  actlog(0, "MKDIR", fpath);

  return 0;
}

static int _mknod(const char *path, mode_t mode, dev_t rdev) 
{
  char fpath[1000];

  if (!strcmp(path, "/")) 
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  } 
  else 
  {
    sprintf(fpath, "%s%s", dirpath, path);
  }
  int res;

  if (S_ISREG(mode)) 
  {
    res = open(fpath, O_CREAT | O_EXCL | O_WRONLY, mode);
    res != -1 ? res = close(res) : (res = -1);
    if (res != -1 )
    {
      res = close(res);
    }    
  } 
  else if (S_ISFIFO(mode)) 
  {
    res = mkfifo(fpath, mode);
  }
  else 
  {
    res = mknod(fpath, mode, rdev);
  }

  if (res == -1) return -errno;
  actlog(0, "CREATE", fpath);
  return 0;
}

static int _unlink(const char *path) 
{
  char fpath[1000];
  if (!strcmp(path, "/")) 
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  } 
  else 
  {
    sprintf(fpath, "%s%s", dirpath, path);
  }
  int res;

  res = unlink(fpath);
  if (res == -1) return -errno;
  actlog(1, "UNLINK", fpath);
  return 0;
}

static int _rmdir(const char *path) 
{
  char fpath[1000];
  sprintf(fpath, "%s%s", dirpath, path);

  int res;

  res = rmdir(fpath);
  if (res == -1) return -errno;
  actlog(1, "RMDIR", fpath);
  return 0;
}

static int _rename(const char *before, const char *after) 
{
  char *mod_before = strstr(before, "module_"); 
  char *mod_after = strstr(after, "module_");

  char f_before[1000], f_after[1000], str[100];

  sprintf(f_before, "%s%s", dirpath, before);
  sprintf(f_after, "%s%s", dirpath, after);

  int res;

  res = rename(f_before, f_after);
  if (res == -1) return -errno;

  if (!mod_before && mod_after) 
  {
    modularize(f_after);
  }
  else if (mod_before && !mod_after) 
  {
    unmod(f_after);
  }

  sprintf(str, "%s::%s", before, after);
  actlog(0, "RENAME", str);
  return 0;
}

static int _truncate(const char *path, off_t size) 
{
  char fpath[1000];
  sprintf(fpath, "%s%s", dirpath, path);
  int res;

  res = truncate(fpath, size);
  if (res == -1) return -errno;
  actlog(0, "TRUNCATE", fpath);
  return 0;
}

static int _open(const char *path, struct fuse_file_info *fi) 
{
  char *modular = strstr(path, "module_");

  char fpath[1000];
  if (modular) 
  {
    sprintf(fpath, "%s%s.000", dirpath, path);
  }
  else 
  {
    sprintf(fpath, "%s%s", dirpath, path);
  }

  int res;
  res = open(fpath, fi->flags);
  if (res == -1) return -errno;
  actlog(0, "OPEN", fpath);
  close(res);
  return 0;
}

static int _read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) 
{
  char fpath[1000];
  sprintf(fpath, "%s%s", dirpath, path);
  int fd = 0;
  int res = 0;

  (void) fi;
  fd = open(fpath, O_RDONLY);
  if (fd == -1) return -errno;

  res = pread(fd, buf, size, offset);
  if (res == -1) res = -errno;
  close(fd);
  actlog(0, "CAT", fpath);
  return res;
}

static int _write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) 
{
  char fpath[1000];
  sprintf(fpath, "%s%s", dirpath, path);
  int fd = 0;
  int res = 0;

  (void) fi;
  fd = open(fpath, O_WRONLY);
  if (fd == -1) return -errno;

  res = pwrite(fd, buf, size, offset);
  if (res == -1) res = -errno;
  close(fd);
  actlog(0, "WRITE", fpath);
  return res;
}

static struct fuse_operations _function = {
  .getattr  = _getattr,
  .readdir  = _readdir,
  .mkdir    = _mkdir,
  .mknod    = _mknod,
  .unlink   = _unlink,
  .rmdir    = _rmdir,
  .rename   = _rename,
  .truncate = _truncate,
  .open     = _open,
  .read     = _read,
  .write    = _write,
};

int main(int argc, char *argv[]) 
{
  umask(0);
  return fuse_main(argc, argv, &_function, NULL);
}