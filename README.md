# Sistem Operasi F - Laporan Resmi Shift 4
Kelas Sistem Operasi F - Kelompok F02

## Anggota Kelompok :
- Salsabila Fatma Aripa (5025211057)
- Arfi Raushani Fikra (5025211084)
- Ahmad Fauzan Alghifari (5025211091)

### Soal 1
**a.** Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

kaggle datasets download -d bryanb/fifa-player-stats-database

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 

**Jawaban A**
Untuk menjawab soal bagian a, akan dibuat fungsi download_extract.
```c
void download_extract() {
    pid_t pid = fork();

    if (pid == 0) {
        // Child process
        system("kaggle datasets download -d bryanb/fifa-player-stats-database && unzip -o fifa-player-stats-database.zip");
        exit(0);
    } else if (pid > 0) {
        // Parent process
        wait(NULL);
    }
}
```

Perlu dicatat bahwa command "kaggle" akan berjalan setelah meng-konfigurasikan instalasi yang tepat untuk menjalankan command "kaggle".

**b.** Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.

**Jawaban B**
Untuk menjawab soal bagian b, akan dibuat fungsi display().
```c
void display() {
    system("awk -F\",\" '($3 <= 25 && $7 >= 75 && $9 != \"Manchester City\") { printf \"%-20s %-20s %-20s %-20s %-20s %-20s\\n\", $2, $9, $3, $8, $5, $4 }' FIFA23_*.csv");
}
```

Fungsi tersebut akan menampilkan data-data yang diperlukan berdasarkan batasan yang sudah disebutkan pada soal.

**Output dari gabungan A dan B**
![image](/uploads/3eec2d43332d7fcc4d9ba97dbe91cc8b/image.png)

**c.** Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan. Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.

**Jawaban C**
*1.* Membuat Dockerfile
```Dockerfile
FROM ubuntu:latest

RUN apt-get update && \
    apt-get install -y \
    gcc \
    make \
    unzip \
    wget


WORKDIR /app

COPY storage.c .

RUN gcc -o storage storage.c

RUN apt-get install -y python3-pip && \
    pip3 install kaggle

ENV KAGGLE_USERNAME=fazghfr
ENV KAGGLE_KEY=8d438407d43216d403d7c2addcc9f0b6

RUN ./storage

CMD ["./storage"]
```

Dapat dilihat bahwa didalam dockerfile ini akan meng-set environtment yang sesuai untuk menjalankan kaggle. Disini digunakan kaggle API, dimana akan menggunakan KAGGLE_USERNAME dan KAGGLE_KEY untuk mengakses proses unduh pada kaggle.

*2.* Membuat docker image 
```
docker build -t fazghfr/storage-app .
```
![image](/uploads/d761e1fa73ed48248308efc73efca53a/image.png)

*3.* Menjalankan docker container menggunakan image yang telah dibuat
```
docker run fazghfr/storage-app
```

![image](/uploads/7049f0f3b41244175892ecdd30debeed/image.png)


**d.** mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia

sebelum menjalankan command dibawah, pastikan sudah ada repository dengan nama repo merupakan nama image yang ingin di-push
```
docker login 
docker push fazghfr/storage-app:latest
```
![image](/uploads/bf7b982f40c97e24e01c0cd66faa88cb/image.png)

**e.** Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.
```yml
version: '3'
services:
  run_test:
    image: fazghfr/storage-app:latest
    deploy:
      replicas: 5
    ports:
      - "8080-8084:8080"
```

docker-compose.yml di atas akan membuat 5 instance dengan port dari 8080-8084. 

Ouput di folder 'barcelona' dan 'napoli':

![image](/uploads/7086b209251ceef06d91634d9abb1a58/image.png)

### Soal 3

Dhafin adalah seorang yang sangat pemalu, tetapi saat ini dia sedang mengagumi seseorang yang dia tidak ingin seorangpun tahu. Maka dari itu, dia melakukan berbagai hal sebagai berikut.

**a.** Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah [/etc](https://drive.google.com/file/d/1xBFLFSOsN-DuQCAkKWxyk4GlFH8SprlT/view) yang bernama secretadmirer.c

**b.** Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64.  Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).

**c.** Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.

**d.** Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan a.k.a terserah).

**e.** Selanjutnya, Dhafin ingin melakukan mounting FUSE tersebut di dalam Docker Container menggunakan **Docker Compose** (gunakan base image Ubuntu Bionic ). Ketentuannya, terdapat dua buah container sebagai berikut.
- Container bernama **Dhafin** akan melakukan mount FUSE yang telah dimodifikasi tersebut. 

- Container bernama **Normal** akan melakukan mount yang hanya menampilkan **/etc** yang normal tanpa ada modifikasi apapun. 

**f.** Kalian masih penasaran siapa yang dikagumi Dhafin? 🥰😍😘😚🤗😻💋💌💘💝💖💗 Coba eksekusi image fhinnn/sisop23 deh, siapa tahu membantu. 🙂

#### Penyelesaian
**a.** Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah [/etc](https://drive.google.com/file/d/1xBFLFSOsN-DuQCAkKWxyk4GlFH8SprlT/view) yang bernama secretadmirer.c

Pertama, Menentukan *dirpath terlebih dahulu untuk file yang akan dilakukan FUSE.

Kedua,Untuk mengimplementasikan FUSE, kita harus menggunakan struct ini dan harus mendefinisikan fungsi yang ada di dalam struct tersebut. Setelahnya, kita mengisi struct tersebut dengan pointer dari fungsi yang ingin diimplementasikan.

```c
#define FUSE_USE_VERSION 28

#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <limits.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

static const char *dirpath = "/home/bilaaripa/praktikum4/inifolderetc/sisop";

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        char name[100];
        strcpy(name, de->d_name);
        char cur_path[1500];
        sprintf(cur_path, "%s/%s", fpath, de->d_name);

        res = (filler(buf, name, &st, 0));

        if (res != 0)
            break;
        if(de->d_type == DT_REG) {
            if(is_encoded(name)) {
                encodeBase64File(cur_path);
            }
            else if(strlen(name) <= 4) {
                convert_to_binary(name);
            }
            else {
                modify_filename(name);
            }
            changePath(cur_path, name);
        }
        else {
            if(strlen(name) <= 4) {
                convert_to_binary(name);
            }
            else {
                modify_directoryname(name);
            }
            changePath(cur_path, name);
            xmp_readdir(cur_path, buf, filler, offset, fi);
        }
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    (void)fi;

    if (strcmp(path, "/") == 0) {
        path = dirpath;
    } else {
        char fpath[1000];
        sprintf(fpath, "%s%s", dirpath, path);
        if (access(fpath, F_OK) == 0) {
            char input_password[100];
            printf("Masukkan password: ");
            fflush(stdout);
            fgets(input_password, sizeof(input_password), stdin);

            size_t len = strlen(input_password);
            if (len > 0 && input_password[len - 1] == '\n')
                input_password[len - 1] = '\0';

            while (strcmp(input_password, password) != 0) {
                printf("Maaf, password salah! Silahkan coba kembali.\nMasukkan password: ");
                fflush(stdout);
                fgets(input_password, sizeof(input_password), stdin);

                len = strlen(input_password);
                if (len > 0 && input_password[len - 1] == '\n')
                    input_password[len - 1] = '\0';
            }
        }
    }

    return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    // Modify the directory name to all uppercase
    char modified_name[256];
    strcpy(modified_name, path);
    for (int i = 0; modified_name[i] != '\0'; i++) {
        modified_name[i] = toupper(modified_name[i]);
    }

    // Create the directory with the modified name
    int res = mkdir(fpath, mode);
    if (res == -1) {
        return -errno;
    }

    return 0;
}

static int xmp_rename(const char *from, const char *to)
{
    char fpath_from[1000];
    char fpath_to[1000];
    sprintf(fpath_from, "%s%s", dirpath, from);
    sprintf(fpath_to, "%s%s", dirpath, to);

    // Modify the destination name to all uppercase
    char modified_name[256];
    strcpy(modified_name, to);
    for (int i = 0; modified_name[i] != '\0'; i++) {
        modified_name[i] = toupper(modified_name[i]);
    }

    // Rename the file/directory with the modified name
    int res = rename(fpath_from, fpath_to);
    if (res == -1) {
        return -errno;
    }

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .open = xmp_open,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
};

int main(int argc, char *argv[])
{
    umask(0);

    // Run the FUSE filesystem
    int fuse_stat = fuse_main(argc, argv, &xmp_oper, NULL);

    return fuse_stat;
}

```
- Fungsi `xmp_getattr` digunakan untuk mendapatkan atribut dari suatu file atau direktori, seperti permissions, owner, dan ukuran file. Fungsi ini memanipulasi path lengkap file atau direktori dan menggunakan fungsi lstat untuk mendapatkan informasi atribut tersebut.

- Fungsi `xmp_readdir` digunakan untuk membaca isi dari suatu direktori. Fungsi ini membuka direktori yang diberikan, membaca setiap entri di dalamnya, dan memperoleh informasi atribut setiap entri. 

- Fungsi `xmp_read` digunakan untuk membaca konten dari suatu file. Fungsi ini membuka file yang diberikan dan menggunakan fungsi pread untuk membaca konten file dari offset yang ditentukan. Setelah membaca, file ditutup dan konten yang dibaca disalin ke buffer yang diberikan.

- Fungsi `xmp_open` digunakan saat membuka suatu file. Fungsi ini melakukan pengecekan jika file yang dibuka adalah direktori utama, maka direktori utama tersebut ditetapkan sebagai direktori yang digunakan dalam sistem file virtual ini. Jika bukan direktori utama, maka diperlukan penginputan password. Jika password yang dimasukkan tidak sesuai dengan password yang ditetapkan, maka pengguna diminta memasukkan password yang benar.

- Fungsi `xmp_mkdir` digunakan untuk membuat direktori baru. Fungsi ini mengubah nama direktori menjadi huruf besar dan kemudian membuat direktori baru dengan nama yang telah diubah.

- Fungsi `xmp_rename` digunakan untuk mengganti nama file atau direktori. Fungsi ini mengubah nama tujuan menjadi huruf besar dan kemudian menggunakan fungsi rename untuk mengubah nama file atau direktori.


**b.** Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64.  Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).

Menggunakan `base64_table`. Tabel ini terdiri dari 64 karakter yang digunakan untuk mengonversi data biner menjadi karakter ASCII yang aman untuk ditransmisikan melalui protokol yang hanya mendukung karakter terbatas.

Tabel ini terdiri dari karakter huruf kapital A-Z, huruf kecil a-z, angka 0-9, dan dua karakter khusus '+' dan '/'. Karakter '+' digunakan untuk representasi nilai 62 dalam encoding Base64, sedangkan karakter '/' digunakan untuk representasi nilai 63.

```c
static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void encodeBase64File(const char *filename) {
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        return;
    }

    // Get file size
    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    // Allocate memory for file content
    uint8_t *file_content = (uint8_t *)malloc(file_size);
    if (file_content == NULL) {
        printf("Memory allocation failed\n");
        fclose(file);
        return;
    }

    // Read file content
    size_t bytes_read = fread(file_content, 1, file_size, file);
    fclose(file);

    if (bytes_read != file_size) {
        printf("Failed to read file: %s\n", filename);
        free(file_content);
        return;
    }

    // Encode file content as Base64
    uint8_t input[3], output[4];
    size_t input_len = 0;
    size_t output_len = 0;
    size_t i = 0;
    
    file = fopen(filename, "wb");
    if (file == NULL) {
        printf("Failed to open file for writing: %s\n", filename);
        free(file_content);
        return;
    }

    while (i < file_size) {
        input[input_len++] = file_content[i++];
        if (input_len == 3) {
            output[0] = base64_table[input[0] >> 2];
            output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
            output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
            output[3] = base64_table[input[2] & 0x3F];

            fwrite(output, 1, 4, file);

            input_len = 0;
            output_len += 4;
        }
    }

    if (input_len > 0) {
        for (size_t j = input_len; j < 3; j++) {
            input[j] = 0;
        }

        output[0] = base64_table[input[0] >> 2];
        output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
        output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
        output[3] = base64_table[input[2] & 0x3F];

        fwrite(output, 1, input_len + 1, file);

        for (size_t j = input_len + 1; j < 4; j++) {
            fputc('=', file);
        }

        output_len += 4;
    }

    fclose(file);
    free(file_content);

    printf("File encoded successfully. file: %s\n", filename);
}

bool is_encoded(const char *name) {
    char first_char = name[0];
    if (first_char == 'l' || first_char == 'L' ||
        first_char == 'u' || first_char == 'U' ||
        first_char == 't' || first_char == 'T' ||
        first_char == 'h' || first_char == 'H') {
        return true;
    }
    return false;
}

```

- `encodeBase64File(const char *filename)`: Fungsi ini membuka file dengan mode baca biner dan membaca konten file. Selanjutnya, konten file dienkripsi menggunakan Base64 dan disimpan kembali ke file yang sama.

- `is_encoded(const char *name)`: Fungsi ini memeriksa apakah suatu nama file telah dienkripsi menggunakan Base64. Jika huruf pertama pada nama file adalah 'l', 'L', 'u', 'U', 't', 'T', 'h', atau 'H', maka fungsi ini mengembalikan nilai true.

**c.** Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.

```c
void modify_filename(char *name)
{
    for (int i = 0; name[i]; i++)
    {
        name[i] = tolower(name[i]);
    }
}

void modify_directoryname(char *name)
{
    for (int i = 0; name[i]; i++)
    {
        name[i] = toupper(name[i]);
    }
}

void convert_to_binary(char *name)
{
    int len = strlen(name);
    char *temp = (char *)malloc((8 * len + len) * sizeof(char)); // Allocate memory for modified string

    int index = 0; // Index for the modified string

    for (int i = 0; i < len; i++)
    {
        char ch = name[i];

        // Convert character to binary string
        for (int j = 7; j >= 0; j--)
        {
            temp[index++] = (ch & 1) + '0'; // Convert bit to character '0' or '1'
            ch >>= 1;                       // Shift right by 1 bit
        }

        if (i != len - 1)
        {
            temp[index++] = ' '; // Add space separator between binary codes
        }
    }

    temp[index] = '\0'; // Null-terminate the modified string

    strcpy(name, temp); // Update the original string with the modified string
    free(temp);         // Free the allocated memory
}

void changePath(char *str1, char *str2) {
    char *slash = strrchr(str1, '/');
    char temp[100];
    strcpy(temp, str1);
    if (slash != NULL) {
        ++slash; // Move past the slash
        strcpy(slash, str2);
        rename(temp, str1);
    }
}

```
- `modify_filename(char *name)`: Fungsi ini mengubah semua karakter dalam nama file menjadi huruf kecil (lowercase).

- `modify_directoryname(char *name)`: Fungsi ini mengubah semua karakter dalam nama direktori menjadi huruf besar (uppercase).

- `convert_to_binary(char *name)`: Fungsi ini mengonversi setiap karakter dalam nama file menjadi representasi biner. Setiap karakter dikonversi menjadi string biner dengan panjang 8 bit dan dipisahkan oleh spasi. Contohnya, karakter 'A' akan dikonversi menjadi "01000001".

- `changePath(char *str1, char *str2)`: Fungsi ini mengubah path (jalur) dari suatu file. Path awal yang disimpan di str1 akan diubah dengan mengganti nama file atau direktori dengan str2. Proses perubahan path dilakukan dengan menggunakan fungsi rename().

FUSE Modifikasi :

![filemodif](/uploads/dd2ebb534268b86a101ef54952d8a4db/filemodif.png)


**d.** Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan a.k.a terserah).

```c
static const char *password = "paus123";

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    (void)fi;

    if (strcmp(path, "/") == 0) {
        path = dirpath;
    } else {
        char fpath[1000];
        sprintf(fpath, "%s%s", dirpath, path);
        if (access(fpath, F_OK) == 0) {
            char input_password[100];
            printf("Masukkan password: ");
            fflush(stdout);
            fgets(input_password, sizeof(input_password), stdin);

            size_t len = strlen(input_password);
            if (len > 0 && input_password[len - 1] == '\n')
                input_password[len - 1] = '\0';

            while (strcmp(input_password, password) != 0) {
                printf("Maaf, password salah! Silahkan coba kembali.\nMasukkan password: ");
                fflush(stdout);
                fgets(input_password, sizeof(input_password), stdin);

                len = strlen(input_password);
                if (len > 0 && input_password[len - 1] == '\n')
                    input_password[len - 1] = '\0';
            }
        }
    }

    return 0;
}

```

- Variabel password yang bertipe data const char * dan diinisialisasi dengan string "paus123". Variabel ini berfungsi sebagai sebuah password yang digunakan untuk memberikan akses terbatas terhadap beberapa operasi pada file sistem yang diimplementasikan.

- Dalam fungsi `xmp_open`, setelah memeriksa apakah path sama dengan "/", program akan meminta pengguna untuk memasukkan password. Pengguna diminta memasukkan password dengan menggunakan fungsi fgets dan password yang dimasukkan akan disimpan dalam array input_password

![4](/uploads/edf3a1ce60628dfcd260867d1ef4ded3/4.png)

**e.** Selanjutnya, Dhafin ingin melakukan mounting FUSE tersebut di dalam Docker Container menggunakan **Docker Compose** (gunakan base image Ubuntu Bionic ). Ketentuannya, terdapat dua buah container sebagai berikut.
- Container bernama **Dhafin** akan melakukan mount FUSE yang telah dimodifikasi tersebut. 

- Container bernama **Normal** akan melakukan mount yang hanya menampilkan **/etc** yang normal tanpa ada modifikasi apapun. 

```yml
version: "3"

services:
    dhafin:
        image: ubuntu:bionic
        container_name: Dhafin
        command: tail -f /dev/null
        volumes:
            - /home/bilaaripa/praktikum4/inifolderetc/sisop:/dhafin

    normal:
        image: ubuntu:bionic
        container_name: Normal
        command: tail -f /dev/null
        volumes:
            - /home/bilaaripa/praktikum4/etc:/normal

```
konfigurasi Docker Compose yang mendefinisikan dua layanan: `"dhafin" dan "normal"`. Kedua layanan ini menggunakan image `"ubuntu:bionic"` dan menjalankan perintah `"tail -f /dev/null"` di dalam kontainer. Layanan "dhafin" terhubung ke volume lokal :
`"/home/bilaaripa/praktikum4/inifolderetc/sisop"`
dengan volume di dalam kontainer "/dhafin".

sedangkan layanan "normal" terhubung ke volume lokal :
`"/home/bilaaripa/praktikum4/etc"` 
dengan volume di dalam kontainer "/normal".

Konfigurasi ini memungkinkan kontainer-kontainer untuk berinteraksi dengan file dan direktori dalam volume yang terhubung.

**f.** Kalian masih penasaran siapa yang dikagumi Dhafin? 🥰😍😘😚🤗😻💋💌💘💝💖💗 Coba eksekusi image fhinnn/sisop23 deh, siapa tahu membantu. 🙂
```c
sudo docker run fhinnn/sisop23
```

Siapakah orang yang dikagumi Dhafin ?, coba baca pada bagian **Output** dibawah ini :

#### Output
1. Compile dan Jalankann **secretadmirer.c** untuk membuat fuse modifikasi **output**
- + Compile :
```c
gcc -Wall `pkg-config fuse --cflags` secretadmirer.c -o secretadmirer `pkg-config fuse --libs`
```
- + Jalankan :
```c
./secretadmirer -f output
```
2. Compile dan Jalankan **asli.c** untuk membuat fuse normal **normal**
- + Compile :
```c
gcc -Wall `pkg-config fuse --cflags` asli.c -o asli `pkg-config fuse --libs`
```
- + Jalankan :
```c
./asli normal
```
3. Lalu jalankan docker-compose
```c
sudo docker compose up -d
```
4. Masuk kedalam container **Dhafin** dan mengecek apakah **/inifolderetc/sisop** sudah ter Fuse sesuai perintah para poin b dan c.
```c
sudo docker exec -it Dhafin /bin/bash
```

![1](/uploads/bb57e5000d2d0ea90306121750c25d24/1.png)
 
 encode sudah terbentuk :
 
 ![2](/uploads/db324cbf211b4e91b19f12f3b2ec096e/2.png)
 
5. Masuk kedalam container **Normal** 
```c
sudo docker exec -it Normal /bin/bash
```

![3](/uploads/793a2809c98ddc867a8d2138480167b7/3.png)

6. Eksekusi image **fhinnn/sisop23** untuk mengetahui siapa yang dikagumi Dhafin.

![5](/uploads/02639f4b4e0730e748e4b8344f546302/5.png)


7. Ditemukan folder yang berbeda bernama **kyaknyasihflag** 

![6](/uploads/715b438e56dd2909903d8cf76befd195/6.png)

8. lalu folder tersebut dibuka dan terdapat file **New Text Document.txt** lalu diibuka.

![7](/uploads/ffe8385366a91c7915adba0e028f70e6/7.png)

9. Setelah dibuka terdapat sebuah kode  dan mari kita convert.

![8](/uploads/58fea86e01d64c86224da7c45191f4e8/8.png)

10. Setelah diconvert maka muncullah sebuah nama orang yang dikagumi dhafin yaitu **h4nif4h_ama12i**

![9](/uploads/28344e9c5f789f8127c7a784f5c3bf63/9.png)


### Soal 4

Pada suatu masa, terdapat sebuah perusahaan bernama Bank Sabar Indonesia yang berada pada masa kejayaannya. Bank tersebut memiliki banyak sekali kegiatan-kegiatan yang  krusial, seperti mengolah data nasabah yang mana berhubungan dengan uang. Suatu ketika, karena susahnya *maintenance*, petinggi bank ini menyewa seseorang yang mampu mengorganisir file-file yang ada di Bank Sabar Indonesia. 
Salah satu karyawan di bank tersebut merekomendasikan Bagong sebagai seseorang yang mampu menyelesaikan permasalahan tersebut. Bagong memikirkan cara terbaik untuk mengorganisir data-data nasabah dengan cara membagi file-file yang ada dalam bentuk **modular**, yaitu membagi file yang mulanya berukuran besar menjadi file-file *chunk* yang berukuran kecil. Hal ini bertujuan agar saat terjadi *error*, Bagong dapat mudah mendeteksinya. Selain dari itu, agar Bagong mengetahui setiap kegiatan yang ada di *filesystem*, Bagong membuat sebuah sistem log untuk mempermudah *monitoring* kegiatan di *filesystem* yang mana, nantinya setiap kegiatan yang terjadi akan dicatat di sebuah file *log* dengan ketentuan sebagai berikut.

- Log akan dibagi menjadi beberapa level, yaitu **REPORT** dan **FLAG**.
- Pada level log **FLAG**, log akan mencatat setiap kali terjadi system call `rmdir` (untuk menghapus direktori) dan `unlink` (untuk menghapus file). Sedangkan untuk sisa operasi lainnya, akan dicatat dengan level log **REPORT**.
- Format untuk logging yaitu sebagai berikut.
`[LEVEL]::[yy][mm][dd]-[HH]:[MM]:[SS]::[CMD]::[DESC ...]`

    **Contoh:**

    REPORT::230419-12:29:28::RENAME::/bsi23/bagong.jpg::/bsi23/bagong.jpeg
    REPORT::230419-12:29:33::CREATE::/bsi23/bagong.jpg
    FLAG::230419-12:29:33::RMDIR::/bsi23

Tidak hanya itu, Bagong juga berpikir tentang beberapa hal yang berkaitan dengan ide modularisasinya sebagai berikut yang ditulis dalam **modular.c.**

**a.**  Pada filesystem tersebut, jika Bagong membuat atau me-*rename* sebuah direktori dengan awalan **module_**, maka direktori tersebut akan menjadi direktori modular.

**b.** Bagong menginginkan agar saat melakukan modularisasi pada suatu direktori, maka modularisasi tersebut juga berlaku untuk konten direktori lain di dalam direktori (subdirektori) tersebut.

**c.** Sebuah file nantinya akan terbentuk bernama **fs_module.log** pada direktori **home** pengguna **(/home/[user]/fs_module.log)** yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan yang telah disebutkan sebelumnya.

**d.** Saat Bagong melakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar `1024 bytes` dan menjadi normal ketika diakses melalui *filesystem* yang dirancang oleh dia sendiri.

**Contoh:**

file **File_Bagong.txt** berukuran `3 kB` pada direktori **asli** akan menjadi 3 file kecil, yakni **File_Bagong.txt.000**, **File_Bagong.txt.001**, dan **File_Bagong.txt.002** yang masing-masing berukuran `1 kB` (1 kiloBytes atau 1024 bytes).

**e.** Apabila sebuah direktori modular di-*rename* menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali *(fixed)*.


#### Penyelesaian

Untuk menyelesaikan permasalahan ini, akan dibuat sebuah *filesystem* yang nantinya akan di-*mount* ke sebuah direktori. Pada *filesystem* ini, nantinya dapat dilakukan operasi modularisasi yang berdampak pada perubahan di direktori *mount*. Selain fungsi-fungsi untuk FUSE lainnya, terdapat tiga fungsi tambahan yang digunakan untuk melakukan modularisasi dan satu fungsi untuk melakukan pencatatan log. Fungsi yang digunakan untuk keperluan modularisasi yaitu `modularize()`, `mod()`, dan `unmod()`. Fungsi `modularize()` bersama fungsi `mod()` akan melakukan pemecahan suatu file ketika nama direktori dari file tersebut memiliki awalan "**module_**". Penamaan ini terjadi saat suatu direktori diganti namanya. Sedangkan fungsi `unmod()` akan melakukan penggabungan kembali potongan-potongan file yang sebelumnya terpecah karena diganti penamaan direktori penyimpanannya. Berikut kode untuk fungsi-fungsi tersebut;

```c
void modularize(char *path)
{
    struct dirent *ep;
    DIR *dp; 
    dp = opendir(path);

    if(!dp) return;

    char dir[1000], file[1000];

    while ((ep = readdir (dp)))
    {
      if (!strcmp(ep->d_name, ".") || !strcmp(ep->d_name, "..")) continue;
      if(ep->d_type == DT_DIR)
      {
        sprintf(dir, "%s/%s", path, ep->d_name);
        modularize(dir);
      }
      else if (ep->d_type == DT_REG)
      {
        sprintf(file, "%s/%s", path, ep->d_name);
        mod(file);
      }
    }
    closedir(dp);
}
```

Pada fungsi `modularize()` tersebut, akan dilakukan penelusuran terhadap suatu direktori yang jalur atau path-nya telah diberikan di argumen fungsi. dengan bantuan `struct dirent`, pointer `DIR`, fungsi `opendir()`, dan `while-loop`, fungsi `modularize()` akan menuju pada suatu file. Saat penelusuran oleh fungsi `modularize()` ini juga dilakukan pemeriksaan apakah sebuah alamat yang dituju (`path`)merupakan sebuah direktori atau file. Jika yang dituju merupakan direktori, maka fungsi `modularize()` akan dipanggil kembali secara rekursif dengan terlebih dahulu menambahkan direktori tersebut ke alamat atau `path` pencarian menggunakan bantuan fungsi `sprintf()`. Adapun jika alamat yang dituju mengarah kepada sebuah file, maka nama file akan ditambahkan ke alamat pencarian dan fungsi `mod()` dipanggil. Berikut kode untuk fungsi `mod()`;

```c
void mod(char *path)
{
    FILE *file = fopen(path, "rb");
    int count = 0;
    char tarpath[2000];

    sprintf(tarpath, "%s.%03d", path, count);
    void *buffer = malloc(1024);

    while (1)
    {
        size_t size = fread(buffer, 1, 1024, file);
        if (!size) break;

        FILE *fetch = fopen(tarpath, "w");
        fwrite(buffer, 1, size, fetch);
        fclose(fetch);
        count++;
        sprintf(tarpath, "%s.%03d", path, count);
    }
    free(buffer);
    fclose(file);
    remove(path);
}
```
Pada fungsi `mod()` ini akan dilakukan pemecahan file menjadi potongan kecil berukuran `1 kb` atau 1024 bytes. Pertama dilakukan pembukaaan file dengan `path` yang diberikan dengan mode baca biner (`"rb"`) dan menghasilkan pointer `file` ke file tersebut. Berikutnya, terdapat inisialisasi variabel `count` untuk menyimpan urutan potongan file, dan sebuah array penampung bernama `tarpath[]`. Setelah itu dilakukan penulisan alamat, nama file, dan urutan potongan file (`count`) ke dalam array `tarpath` dengan bantuan fungsi `sprintf()`. Urutan potongan file memiliki format angka tiga digit dan diisi dengan nol di depan jika angka urutan kurang dari tiga digit. Setelah itu, fungsi `mod()` mengalokasikan memori sebesar 1024 bytes menggunakan fungsi `malloc()` dan menyimpan alamat memori tersebut dalam pointer `buffer`. Pointer ini akan digunakan untuk menyimpan data yang akan dibaca dari file. Selanjutnya dengan menggunakan `while-loop`, dilakukan pembacaan data sebesar 1024 bytes dari file yang sedang dibuka menggunakan fungsi `fread()` dan menyimpan jumlah byte yang berhasil dibaca dalam variabel `size`. Berikutnya membuka file dengan path `tarpath` dalam mode tulis ("`w`") dan menghasilkan pointer `fetch` ke file tersebut. Lalu, `fwrite(buffer, 1, size, fetch);` akan menulis data yang telah dibaca dari file sebelumnya ke file yang baru dibuka menggunakan fungsi `fwrite()`. Setelah itu pointer `fetch` ditutup, variabel `count` di-*increment*, dan `tarpath` diperbarui dengan format string yang baru berdasarkan `path` dan `count` yang sudah diubah. Terakhir, mengosongkan memori yang telah dialokasikan sebelumnya menggunakan `malloc()` dengan memanggil fungsi `free()`, menutup `file` yang dibuka pada awal fungsi dengan perintah `fclose(file);`, dan menghapus file asli dengan path `path`.

Selain melakukan pemecahan file atau modularisasi, terdapat fungsi `unmod()` yang digunakan untuk menggabung kembali potongan-potongan file. Berikut kode untuk fungsi `unmod()`;

```c
void unmod(char *path)
{
    DIR *dir = opendir(path);
    struct dirent *entry;
    struct stat sb;
    while ((entry = readdir(dir)))
    {
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

        char tarpath[300];
        snprintf(tarpath, sizeof(tarpath), "%s/%s", path, entry->d_name);
        if (stat(tarpath, &sb) == -1) continue;

        if (S_ISDIR(sb.st_mode))
        {
            unmod(tarpath);
        }
        else if (S_ISREG(sb.st_mode) && strlen(tarpath) > 3 && !strcmp(tarpath + strlen(tarpath) - 3, "000"))
        {
            int count = 0;
            char dest[300], temp[400];
            memset(dest, '\0', sizeof(dest));
            memset(temp, '\0', sizeof(temp));
            strncpy(dest, tarpath, strlen(tarpath) - 4);
                
            while(1)
            {
                if (count < 10) snprintf(temp, sizeof(temp), "%s.00%d", dest, count);
                else if (count < 100) snprintf(temp, sizeof(temp), "%s.0%d", dest, count);
                else snprintf(temp, sizeof(temp), "%s.%d", dest, count);

                if (stat(temp, &sb)) break;
                FILE *destfile = fopen(dest, "ab");
                FILE *tempfile = fopen(temp, "rb");
                if (destfile && tempfile) 
                {
                    char buffer[1024];
                    size_t read_bytes;
                    while ((read_bytes = fread(buffer, 1, sizeof(buffer), tempfile)) > 0) 
                    {
                        fwrite(buffer, 1, read_bytes, destfile);
                    }
                }
                if (tempfile) fclose(tempfile);
                if (destfile) fclose(destfile);
                remove(temp);
                count++;
            }
        }
    }
    closedir(dir);
}
```

Fungsi `unmod()` ini mengambil parameter berupa `path` atau alamat dari sebuah file. Di dalam fungsi ini juga dilakukan penelusuran direktori dengan bantuan `struct dirent`, pointer `DIR`, fungsi `opendir()`, dan `while-loop`. Pada fungsi ini juga menggunakan array karakter `tarpath` (deklarasi baru) dengan ukuran 300. Array ini akan digunakan untuk menyimpan path entri yang sedang dibaca. Fungsi ini juga dibantu `struct stat` bernama `sb` yang digunakan untuk menyimpan informasi mengenai atribut file atau direktori. `sb` ini digunakan untuk mengecek atribut file atau direktori dari `tarpath` menggunakan fungsi `stat()`. Jika pengambilan atribut gagal (mengembalikan nilai -1), maka lanjut ke iterasi selanjutnya untuk membaca entri berikutnya. Selanjutnya, terdapat pemeriksaan apakah entri saat ini adalah sebuah direktori menggunakan fungsi `S_ISDIR()` dan atribut `st_mode` dari `sb`. Jika entri saat ini adalah sebuah direktori, maka fungsi `unmod()` akan dipanggil secara rekursif untuk membaca direktori tersebut. Jika  entri saat ini adalah sebuah file reguler dengan ekstensi `".000"`. Kondisi ini memeriksa apakah `tarpath` adalah file reguler (bukan direktori) dan panjangnya lebih dari 3 karakter (minimal panjang nama file adalah 4) serta apakah tiga karakter terakhirnya adalah "000". Jika demikian, maka akan dilakukan proses penggabungan. Proses penggabungan diawali dengan 
- mendeklarasikan dan menginisialisasi variabel `count` dengan nilai 0. Variabel ini akan digunakan untuk menyimpan nomor urutan file yang akan digabungkan, 

- array karakter `dest` dengan ukuran 300 dan `temp` dengan ukuran 400. Array ini akan digunakan untuk menyimpan `path` file tujuan (`dest`) dan path file sementara (`temp`) yang digunakan saat proses penggabungan.

Kedua array `dest` dan `temp` dikosongkan dengan menggunakan fungsi `memset()` yaitu pada perintah `memset(dest, '\0', sizeof(dest));` dan `memset(temp, '\0', sizeof(temp));`. Selanjutnya, dilakukan penyalinan `tarpath` ke `dest`, kecuali empat karakter terakhir, sehingga menghapus ekstensi "`.000`" dari `tarpath`. Berikutnya di dalam `while-loop`, dilakukan pemeriksaan jika `count` kurang dari 10, maka akan membentuk nama file sementara `temp` dengan format "`dest.00[count]`" . Namun jika count kurang dari 100, maka akan membentuk nama file sementara `temp` dengan format "`dest.0[count]`", dan jika `count` lebih dari atau sama dengan 100, maka nama file sementara akan dibentuk pada `temp` dengan format "`dest.[count]`" . Berikutnya, diperiksa, apakah file sementara `temp` ada atau tidak. Jika tidak ada, maka akan `break`. Setelah itu, dilakukan pembukaan file tujuan `dest` dalam mode tambah biner (`"ab"`) dan membuka file sementara `temp` dalam mode baca biner (`"rb"`). Menghasilkan pointer `destfile` dan `tempfile` ke masing-masing file. Di saat kedua pointer menyatakan file dapat dibuka, maka fungsi akan membaca data sebesar ukuran `buffer` (1024 byte) dari file sementara menggunakan fungsi `fread()` dan menyimpan jumlah byte yang berhasil dibaca dalam `read_bytes`. Loop ini akan berjalan selama masih ada data yang berhasil dibaca. Variabel `read_bytes` merupakan variabel bertipe `size_t` yang digunakan untuk menyimpan jumlah byte yang berhasil dibaca dari file sementara. Di dalam `loop`, data yang telah dibaca dari file sementara akan ditulis ke file tujuan menggunakan fungsi `fwrite()`. Setelah semua proses selesai, program akan menutup pointer file sementara dan file tujuan dengan menggunakan bantuan fungsi `fclose()`. Terakhir, isi dari `temp` dihapus, hitungan `count` di-*increment* untuk urutan file yang akan digabung berikutnya, dan menutup direktori pada awal fungsi dengan perintah `closedir(dir);`.

Untuk pencatatan log, digunakan sebuah fungsi yaitu `actlog()`. Fungsi ini menggunakan indikator yang memisahkan level dari suatu perintah, yaitu **FLAG** dan **REPORT**. Pada fungsi ini dilakukan pem-*format*-an waktu eksekusi suatu perintah dan menuliskannya ke sebuah file log yang beralamatkan `/home/user/fs_module.log`. Beriikut kode untuk fungsi tersebut;

```c
void actlog(int flag, char *cmd, const char *desc)
{
    char timebuffer[80], mode[7];
    FILE *prog_log = fopen("/home/arfikra/fs_module.log", "a");

    time_t tnow = time(NULL);
    struct tm *time = localtime(&tnow);

    if(!flag) 
    {
        strcpy(mode, "REPORT");
    }
    else
    {
        strcpy(mode, "FLAG");
    }
    strftime(timebuffer, 90, "%y%m%d-%H:%M:%S", time);
    fprintf(prog_log, "%s::%s::%s::%s\n",mode,timebuffer, cmd, desc);

    fclose(prog_log);
}
```

Di awal kode program ini, kita perlu menentukan direktori untuk *mount* yaitu sebagai berikut
`/home/user/Documents`.

#### Output

![1](/uploads/651a895cc0e93149f76dd50768407c16/1.png)

![2](/uploads/e6f122c613f829642a322484a129135d/2.png)

![3](/uploads/52dd898a5098ba360d2c35575d30be78/3.png)

![4](/uploads/c96cd26960e298867b3ccc0ee04acc33/4.png)



### Revisi

##### Soal 3
1. Melengkapi dan membuat encode sesuai perintah pada soal
2. Memperbaiki password saat membuka file
3. Membuat docker-compose.yml dimana akan membuat container **Dhafin** dan **Normal**
4. Melakukan intruksi sesuai soal untuk mendapatkan nama yang dikagumi oleh Dhafin.

##### Soal 4
1. Merubah fungsi unmod dan melakukan penelusuran direktori dari fungsi tersebut secara langsung
2. Melakukan penelusuran direktori dengan fungsi `modularize()` sebelum menggunakan fungsi `mod()`

### Kendala

Masih terdapat kekurangan dalam pemahaman terhadap FUSE.


